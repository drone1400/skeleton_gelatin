//main.c run when dink is started

void main()
{
 //let's initialize all our globals first.........
 // These globals are REQUIRED by dink.exe (it directly uses them)

 make_global_int("&exp",0);
 make_global_int("&strength", 1);
 make_global_int("&defense", 0);
 make_global_int("&cur_weapon", 0);
 make_global_int("&cur_magic", 0);
 make_global_int("&gold", 0);
 make_global_int("&magic", 0);
 make_global_int("&magic_level", 0);
 make_global_int("&vision", 0);
 make_global_int("&result", 0);
 make_global_int("&lifemax", 10); 
 make_global_int("&life", 10);
 make_global_int("&level", 1);
 make_global_int("&player_map", 0);
 make_global_int("&last_text", 0);
 make_global_int("&update_status", 0);
 make_global_int("&missile_target", 0);
 make_global_int("&missle_source", 0);
 make_global_int("&enemy_sprite", 0);
 make_global_int("&magic_cost", 0);
 
 //the 2 below aren't really required, but used for many default scripts, so best to leave them here.
 make_global_int("&save_x", 0);
 make_global_int("&save_y", 0);

 //Add your own globals here - Best to stick to the total of 200 for compatibility in all Dink Engines
 make_global_int("&story", 0);

 //If Dink had herb boots armed, let's reset his speed.
 set_dink_speed(3)
 sp_frame_delay(1,0)

 //If Dink had a weapon armed, let's reset the graphics to fists.
 init("load_sequence_now graphics\dink\walk\ds-w1- 71 43 38 72 -14 -9 14 9")
 init("load_sequence_now graphics\dink\walk\ds-w2- 72 43 37 69 -13 -9 13 9")
 init("load_sequence_now graphics\dink\walk\ds-w3- 73 43 38 72 -14 -9 14 9")
 init("load_sequence_now graphics\dink\walk\ds-w4- 74 43 38 72 -12 -9 12 9")
 init("load_sequence_now graphics\dink\walk\ds-w6- 76 43 38 72 -13 -9 13 9")
 init("load_sequence_now graphics\dink\walk\ds-w7- 77 43 38 72 -12 -10 12 10")
 init("load_sequence_now graphics\dink\walk\ds-w8- 78 43 37 69 -13 -9 13 9")
 init("load_sequence_now graphics\dink\walk\ds-w9- 79 43 38 72 -14 -9 14 9")
 init("load_sequence_now graphics\dink\idle\ds-i2- 12 250 33 70 -12 -9 12 9")
 init("load_sequence_now graphics\dink\idle\ds-i4- 14 250 30 71 -11 -9 11 9")
 init("load_sequence_now graphics\dink\idle\ds-i6- 16 250 36 70 -11 -9 11 9")
 init("load_sequence_now graphics\dink\idle\ds-i8- 18 250 32 68 -12 -9 12 9")
 init("load_sequence_now graphics\dink\hit\normal\ds-h2- 102 75 60 72 -19 -9 19 9")
 init("load_sequence_now graphics\dink\hit\normal\ds-h4- 104 75 61 73 -19 -10 19 10")
 init("load_sequence_now graphics\dink\hit\normal\ds-h6- 106 75 58 71 -18 -10 18 10")
 init("load_sequence_now graphics\dink\hit\normal\ds-h8- 108 75 61 71 -19 -10 19 10")
 
 kill_this_task();
}
