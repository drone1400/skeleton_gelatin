# skeleton_gelatin

This is a DMOD skeleton for the game Dink Smallwood.
If you have no idea what either of these things are, this repo is not for you.
For more info: https://www.dinknetwork.com/

## How to use

This is a JSON-ified verson of RobJ's "Skeleton Gelatin" that is available here: https://www.dinknetwork.com/file/skeleton_gelatin/

You can load it using WDED3 (https://gitlab.com/drone1400/wdep2)

Either download a zip from this gitlab page or clone it with git:

`git clone https://gitlab.com/drone1400/skeleton_gelatin`

Alternatively, you can fork it into your own repo and sync your DMOD work there!

If you're unfamiliar with using git, you can check out some GUI program like SourceTree: https://www.sourcetreeapp.com/

## Simple Example

First, download the repo:
![](.doc/skeleton-example-1.png)

Next, extract it to your DMOD development folder!

Afterwards, simply open it in WDED3 like this:
![](.doc/skeleton-example-2.png)
![](.doc/skeleton-example-3.png)

The DMOD Skeleton should load and your editor should look like this:
![](.doc/skeleton-example-4.png)

Happy DMOD-ing!

